package com.xwintop.xJavaFxTool.utils.func;

/**
 * Code that Changed the World
 *
 * @author Pro
 * @date 2022/3/19
 */
public interface HandleFunc<E> {

    void handle(E e);

}
